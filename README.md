# Download, bag, and archive files from the Internet Archive

Please see the background discussion about this script on the related [Google Doc](https://docs.google.com/document/d/1lC0mgAR-Yr6BK6QJk7AiKwcy3PSdvjQKYBoxSpaed0s/edit?usp=sharing).

## Overview

This code from December 2019 is intended to supersede the old ["download_masters" script](https://bitbucket.org/jeffgerhard1/digital-initiatives/src/master/ia_download_arch_masters/). 

### Goals:

- Identify files that need to be preserved by parsing IA metadata for Scribed books
- Download uncropped jp2s, cropped and OCR'd PDFs, and some metadata files
- Bag these files
- Track the physical disk drive where they will reside.

### Set-up

- For now, this requires a textfile list of items to download, placed in a "files" directory and called `batch1.txt`. [Here](https://drive.google.com/file/d/1ZHJYOO_SVEpHdrjEoHMq9Vw1nNLWAgd9/view?usp=sharing) is a starter batch (derived from our digitization database).
- Also, there needs to be a `finished.txt` file in the files directory. 
- Install the required Python modules.
- Tweak the code if needed.

Later I'll try to make this more robust.

## Requirements

Python 3, including non-standard modules:

- [internetarchive](https://github.com/jjjake/internetarchive)
- [bagit-python](https://github.com/LibraryOfCongress/bagit-python)
