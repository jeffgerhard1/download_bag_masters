'''
iteration 2:
get this working on the FRED computer without direct database accesss

techniques for this:
- start with just a text file listing UIDs
  --> start with ones that we had downloaded from IA in the past so we can clear
        them off of the holdings directory
- later, use a more robust tracking mechanism:
    - remote desktop or similar
    - google sheets or similar
    - gcs (could keep a working document of UIDs there)
        - and could access it via cloudberry drive maybe

can monitor local disk usage via https://docs.python.org/3/library/shutil.html#shutil.disk_usage
can get disk name via https://stackoverflow.com/a/15817469 

'''

# import modules

import internetarchive as ia
import bagit
from pathlib import Path
from datetime import datetime
import win32api

# get a list of items from the textfile

to_download = Path('files').joinpath('batch1.txt').read_text().splitlines()
finished_file = Path('files').joinpath('finished.txt').resolve()
finished = finished_file.read_text().splitlines()

# if we set up the drive with a name I could grab it like this:
# idrive = win32api.GetVolumeInformation("I:\\")[0]

# but if not we should give it some kind of id:
disk = 'ia0001'

download_dir = Path('I://')

for uid in to_download:
    download = True
    destdir = download_dir / uid
    if destdir.exists():
        print(f'\nDirectory already exists for {uid}.\n')
        try:
            bag = bagit.Bag(str(destdir))  # bagit module does not accept Path objects here so converting it to a string
            print(f'({uid} is already a valid bag.)')
            # and ask user if they want to update the database? 
            download = False
        except bagit.BagError:
            print('** WARNING: This is not a bagged directory.')
            download = False
            # Eventually come back to this!
    elif uid in finished:
        print(f'{uid} was marked as already downloaded.')
        download = False
    if download is True:
        destdir.mkdir(exist_ok=True)
        item_files = ia.get_files(uid)
        found = list() # save this files found to add to database later
        for item_file in item_files:
            item_data = list()
            if item_file.format in ['Text PDF', 'Scandata', 'Contents', 
                                    'Single Page Original JP2 Tar']:
                item_file.download(destdir=destdir, verbose=True, checksum=True,
                                    retries=7)
                found.append(item_file.name)
            elif item_file.format == 'Metadata':  # we don't want every file marked as metadata so check it more carefully
                if item_file.name.endswith('meta.xml'):
                    item_file.download(destdir=destdir, verbose=True, checksum=True,
                                        retries=7)
                    found.append(item_file.name)

        # then go ahead and bag these
        bag = bagit.make_bag(destdir, checksum=['md5'])

        finished.append(uid)
        finished_file.write_text('\n'.join(finished))