'''
go through the database and find the books that we need to download

(on early runs this will be a massive list so we have to limit the
amount somehow)

iteration 1:
working on my local machine with database (vpn) access

steps:
- identify books to download from the database
- download via internetarchive module functions
   (see https://archive.org/services/docs/api/internetarchive/internetarchive.html#internetarchive.File)
- generate a manifest file
- bag
- update database

iteration 2:
get this working on the FRED computer without direct database accesss

techniques for this:
- remote desktop or similar
- google sheets or similar
- gcs (could keep a working document of UIDs there)
    - and could access it via cloudberry drive maybe

NOTE: prioritize the ones on the z drive over the ones not yet downloaded

can get disk_usage via https://docs.python.org/3/library/shutil.html#shutil.disk_usage
can get disk name via https://stackoverflow.com/a/15817469 

'''

# import modules

import internetarchive as ia
import pyodbc
import bagit
import hashlib
from pathlib import Path
from datetime import datetime
import os

# set up odbc connection

dsn = 'DSN=Digitization-RDS;Trusted_Connection=Yes;APP=Python;DATABASE=digitization'
cnxn = pyodbc.connect(dsn)
cursor = cnxn.cursor()

# some sql to identify Scribed books
#       (BUT WHAT ARE WE DOING WITH KABIS/ZEUTSCHEL BOOKS???)

sql = '''   SELECT UID
            FROM Item_Entry
            WHERE (Digitization_equipment=? OR Digitization_equipment=?)
            AND
            Format_of_access=?;
            '''
cursor.execute(sql, (1, 8, 1, ))
rows = cursor.fetchall()
all_scribed_pdfs = [_[0] for _ in rows]

disk = 'Jeff test 2019-12'

# ok also think about dark ones if any, and eventually check the new Digital_Preservation table to compare and see if it's already done

sql = 'SELECT Pres_UID, orig_jp2_tar FROM Digital_Preservation'
cursor.execute(sql)
rows = cursor.fetchall()
for row in rows:
    if row[1] is True and row[0] in all_scribed_pdfs:
        all_scribed_pdfs.remove(row[0])

# come back to the above later... there are better ways to import the whole table

download_dir = Path('E:/digi_pres')

for uid in all_scribed_pdfs[0:2]:
    download = True
    destdir = download_dir / uid
    if destdir.exists():
        print(f'\nDirectory already exists for {uid}.\n')
        try:
            bag = bagit.Bag(str(destdir))  # bagit module does not accept Path objects here so converting it to a string
            print(f'({uid} is already a valid bag.)')
            # and ask user if they want to update the database? 
            download = False
        except bagit.BagError:
            print('** WARNING: This is not a bagged directory.')
            download = False
            # Eventually come back to this!
    if download is True:
        destdir.mkdir(exist_ok=True)
        item_files = ia.get_files(uid)
        found = list() # save this files found to add to database later
        for item_file in item_files:
            item_data = list()
            if item_file.format in ['Text PDF', 'Scandata', 'Contents', 
                                    'Single Page Original JP2 Tar']:
                item_file.download(destdir=destdir, verbose=True, checksum=True,
                                    retries=7)
                found.append(item_file.name)
            elif item_file.format == 'Metadata':  # we don't want every file marked as metadata so check it more carefully
                if item_file.name.endswith('meta.xml'):
                    item_file.download(destdir=destdir, verbose=True, checksum=True,
                                        retries=7)
                    found.append(item_file.name)

        # then go ahead and bag these
        bag = bagit.make_bag(destdir, checksum=['md5'])

        # update database (later of course I will have to check this first)
        sql = '''INSERT INTO Digital_Preservation(Pres_UID, Local_disk, 
                Bagged_date, orig_jp2_tar, pdf, meta_xml, scandata_xml, toc_xml)
                VALUES (?, ?, ?, ?, ?, ?, ?, ?);'''
        orig_jp2_tar, pdf, meta_xml, scandata_xml, toc_xml = False, False, False, False, False
        for f in found:
            if f.endswith('orig_jp2.tar'):
                orig_jp2_tar = True
            elif f.endswith('.pdf'):
                pdf = True
            elif f.endswith('meta.xml'):
                meta_xml = True
            elif f.endswith('scandata.xml'):
                scandata_xml = True
            elif f.endswith('toc.xml'):
                toc_xml = True
        cnxn = pyodbc.connect(dsn)
        cursor = cnxn.cursor()
        cursor.execute(sql, (uid, disk, datetime.utcnow(), orig_jp2_tar, pdf, meta_xml, scandata_xml, toc_xml))
        cursor.commit()